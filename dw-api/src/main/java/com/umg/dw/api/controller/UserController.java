/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.umg.dw.api.controller;

import com.umg.dw.api.repository.UserRepository;
import com.umg.dw.core.entities.User;
import io.swagger.annotations.Api;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author luisc
 */
@Api(value = "/", description = "REST User")
@RestController
@RequestMapping("/user")
public class UserController {
    
    @Autowired
    UserRepository userRepository;

    @CrossOrigin
    @RequestMapping(
            value = "/all",
            method = RequestMethod.GET,
            produces = "application/json")
    public List<User> getAll() {
        return (List<User>) userRepository.findAll();
    }

    @RequestMapping(
            value = "/",
            method = RequestMethod.POST,
            produces = "application/json")
    public User create(@RequestBody User user) {
        user = userRepository.save(user);
        return user;
    }


    @RequestMapping(
            value = "/",
            method = RequestMethod.PUT,
            produces = "application/json")
    public User update(@RequestBody User user) {
        user = userRepository.save(user);
        return user;
    }
    
}
